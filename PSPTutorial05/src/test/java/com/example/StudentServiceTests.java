package com.example;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests {
	@Autowired
	StudentService service;
	
	@Test
	public void testSelectAllStudents(){
		List<StudentModel> students = service.selectAllStudents();
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertTrue("Gagal, students isinya cuman satu biji", students.size()>1);
	}
	
	@Test
	public void testSelectStudent(){
		String npm = "123";
		StudentModel student = service.selectStudent(npm);
		Assert.assertNotNull("Gagal, student null", student);
		Assert.assertEquals("Gagal, namanya bukan Chanek", "Chanek", student.getName());
		Assert.assertEquals("Gagal, GPA-nya ga sesuai harapan", 3.55, student.getGpa(), 0.0);
	}
	
	@Test
	public void testCreateStudent(){
		StudentModel student = new StudentModel("126", "Uvuvwevwevwe Onyetenyevwe Ugwemubwem Ossas", 3.99, null);
		Assert.assertNull("Gagal,  npm-nya udah ada yang pake", service.selectStudent(student.getNpm()));
		service.addStudent(student);
		Assert.assertNotNull("Gagal dimasukin", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent(){
		StudentModel student = service.selectStudent("123");
		Assert.assertNotNull("Studentnya null bro", student);
		student.setName("Uvuvwevwevwe Onyetenyevwe Ugwemubwem Ossas");
		service.updateStudent(student);
		Assert.assertEquals("Namanya cuy", student.getName(), service.selectStudent("123").getName());
	}
	
	@Test
	public void testDeleteStudent(){
		StudentModel student = service.selectStudent("123");
		Assert.assertNotNull("Ga ada studentnya bro, null bro", student);
		service.deleteStudent("123");
		student = service.selectStudent("123");
		Assert.assertNull("Lah? Ada studentnya tuh bro", student);
	}
}
